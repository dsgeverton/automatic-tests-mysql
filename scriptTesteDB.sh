#!/bin/bash
# set -e

SCRIPTNAME="$0"
FLAG=0

function usage ()
{
  printf "\n Mysql Test Tool\n"
  printf " usage: ${SCRIPTNAME} [-PARAMETER] value (case exist)\n\n"
  printf " \t-b (database) = Create database backup\n"
  printf " \t-c = Test Connection with server\n"
  printf " \t-I (filename) = Import script database\n"
  printf " \n DML PARAMETERS:\n"
  printf " \t-i (database) = Insert Tests\n"
  printf " \t-u (database) = Update Tests - Not implemented\n"
  printf " \t-d (database) = Delete Tests - Not implemented\n"
  printf " \t-s (database) = Select Tests\n"
  printf " \n\t-h = This help text\n"
  printf " \n\t Script developed by Everton Gonçalves\n\n"
}

is_root() {
  if [[ $EUID -ne 0 ]]; then
    echo -e "${RED}Please run as root"${NC} 2>&1
    exit 1
  fi
}

function connection_db ()
{
  printf "Usuário MYSQL: " && read LOGIN
  printf "Senha do usuário ${LOGIN}: " && read -s PASSWORD > /dev/null
  printf "\nTrying connection Mysql" && printf "." && sleep 1 && printf "." && sleep 1 && printf ". " && sleep 1
  mysql -u ${LOGIN} -p${PASSWORD} -e "" > /dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    printf "OK\n\n"
    export USERSQL=${USER}
    export PASSWORDSQL=${PASSWORD}
  else
     printf "FAIL!\n\n"
     exit 1
   fi
}

function import_database ()
{
  connection_db
  if [[ "${LOGIN}" != "" ]]; then
    mysql -u ${LOGIN} -p${PASSWORD} < ${FILE} > /dev/null 2>&1
    [[ $? -eq 0 ]] && echo "DATABASE IMPORT OK" || echo "ERROR IMPORT"
  else
    echo "Connection with MYSQL not establish!"
  fi
}

function backup ()
{
  is_root
  connection_db
  DATE="$(date +'%d-%m-%Y')";
  HOUR="$(date +'%T')"
  BACKUPNAME="databackup-${HOUR}-${DATE}.sql"
  mysqldump -u ${LOGIN} -p${PASSWORD} ${DATABASE} > ${BACKUPNAME} 2>&1
  if [[ $? -eq 1 ]]; then
    echo "Erro ao efetuar Backup! O banco de dados está correto?"
    exit 1
  else
    echo "Backup local efetuado com sucesso!"
  fi
  read -p "Gostaria de fazer o agendamento de backup? [S/n]: " opc
  if [[ "${opc,,}" != 's' ]]; then
    echo "Backup finalizado!"
  else
    sudo cat << EOF >> /etc/crontab
0 4,11-12,18 * * 1-6 mysqldump -u root -proot db_estoque > ~/home/${BACKUPNAME}
EOF
  [[ $? -eq 0 ]] && printf "Tarefa agendada.\nAgendamento em > /etc/crontab\n\n" || echo "Erro no agendamento de tarefa"
  fi
}

function insert_tests ()
{
  connection_db
  CLEAR="> /dev/null 2>&1"
  read -p "Insira a quantidade de loops: " LOOP
  while [[ ${LOOP} -gt 9 ]]; do
    read -p "For now, we only accept numbers with 1 digit. Enter a new number: " LOOP
  done
  ACCESS="mysql -u ${LOGIN} -p${PASSWORD} -e "
  for (( i = 1; i <= ${LOOP}; i++ )); do
    ${ACCESS}"insert into categoria (descricao) values ('item $i');" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "1." || FLAG=1
    ${ACCESS}"insert into funcionario (cpf, nome, dataNascimento, permissao, rua, numero, bairro, cidade, uf, cep, complemento, ddi, ddd, telefone, usuario, senha, email) values (${i}3${i}2$i$i$i, 'Funcionario $i', '199$i-03-1$i', 1, 'Rua $i', $i$i$i, 'Bairro $i', 'Cidade $i', 'RJ', ${i}2$i$i, 'Complemento $i', 55, 22, 11$i$i, 'user$i', 1$i$i$i, 'email@gmail');" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "2." || FLAG=1
    ${ACCESS}"insert into cliente (nome, cpf, dataNascimento, rua, numero, bairro, cidade, uf, cep, complemento, ddi, ddd, telefone) values ('Cliente $i', $i$i$i$i$i$i$i, '199$i-03-1$i', 'Rua Fictícia $i', '$i$i', 'Bairro $i', 'Cidade $i', 'RJ', $i$i$i$i$i$i$i, 'Complemento $i', 55, 22, 9987${i}5$i${i}2);" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "3." || FLAG=1
    ${ACCESS}"insert into fornecedor (cnpj, razao, rua, numero, bairro, cidade, uf, cep, complemento, ddi, ddd, telefone, email) values ('1901313$i', 'Empresa $i', 'Rua Fictícia $i', 7$i, 'Bairro $i', 'Cidade $i', 'RJ', 23$i$i$i$i${i}3,'Complemento $i', 55, 21, 395922$i$i, 'contato@empresa${i}.com.br');" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "4." || FLAG=1
    ${ACCESS}"insert into produto (idCategoria, idFornecedor, idFuncionario, nome, descricao, precoCusto, precoVenda, quantidade, quantidadeEstoque, quantidadeMinima) values ($i, $i, $i, 'Produto $i', 'Descrição do produto $i', '$i.50', '$i.00', $i$i, 6$i, 10);" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "5." || FLAG=1
    ${ACCESS}"insert into venda (idCliente, idFuncionario, idProduto, quantidade, valor, dataVenda) values ($i, $i, $i, $i, '2$i.00', '2018-0$i-13');" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "6." || FLAG=1
    ${ACCESS}"insert into caixa (idProduto, idVenda, precoUnitario, data) values ('$i', '$i', '$i.00', '2018-0$i-13');" ${DATABASE} > /dev/null 2>&1; [[ $? -eq 0 ]] && printf "7." || FLAG=1
    [[ ${FLAG} -eq 0 ]] && echo "Loop $i: Insertion OK" || echo "Loop $i: Insertion FAIL"
  done
}

function select_tests ()
{
  connection_db
  ACCESS="mysql -u ${LOGIN} -p${PASSWORD} -e "
  ${ACCESS}"show tables;" ${DATABASE} > view_tables.txt 2>&1
  cat view_tables.txt && rm -rf view_tables.txt

  read -p "Insira o nome da tabela que deseja visualizar: " TABLE
  echo ""
  ${ACCESS}"select * from ${TABLE}" ${DATABASE} > view_${TABLE}.txt 2>&1
  cat view_${TABLE}.txt
  echo ""
  read -p "Deseja APAGAR o arquivo de saída desta view?[S/n] :" opc
  if [[ "${opc,,}" == "s" ]]; then
    rm -rf view_${TABLE}.txt
    echo "Arquivo excluído!"
  else
    exit 0;
  fi
}

function main ()
{
  which mysql > /dev/null
  [[ $? -eq 1 ]] && echo "Mysql not installed"
  while getopts "s:i:I:b:ch" opc; do
    case $opc in
      I)
        FILE=${OPTARG}
        import_database ;;
      h) usage ;;
      c) connection_db ;;
      i)
        DATABASE=${OPTARG}
        insert_tests ;;
      b)
        DATABASE=${OPTARG}
        backup ;;
      s)
        DATABASE=${OPTARG}
        select_tests ;;
      *)
        echo "Command not found."
        echo "Use ${SCRIPTNAME} -h parameter for help you" ;;
    esac
    exit 0
  done
  usage
}
 main $@
#
# GRANT usage ON *.* TO everton@localhost IDENTIFIED BY "password";
# GRANT CREATE, DROP, SELECT, INSERT, UPDATE, DELETE ON db_estoque.* TO everton@localhost;
#
# INSERT INTO categoria (nome, descricao, precoCusto) value ('teste2','teste2','teste2');
