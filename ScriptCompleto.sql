CREATE DATABASE IF NOT EXISTS `db_estoque` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_estoque`;

CREATE TABLE IF NOT EXISTS `categoria` (
  `idCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `funcionario` (
  `idFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `dataNascimento` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `permissao` int(11) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `senha` varchar(150) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `complemento` varchar(100) NOT NULL,
  `numero` int(5) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `ddi` varchar(3) NOT NULL,
  `ddd` varchar(3) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  PRIMARY KEY (`idFuncionario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  `rua` varchar(50) NOT NULL,
  `numero` int(5) NOT NULL,
  `complemento` varchar(100) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `ddi` varchar(3) NOT NULL,
  `ddd` varchar(3) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fornecedor` (
  `idFornecedor` int(11) NOT NULL AUTO_INCREMENT,
  `cnpj` int(11) NOT NULL,
  `razao` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `rua` varchar(50) NOT NULL,
  `numero` int(5) NOT NULL,
  `complemento` varchar(100) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `cidade` varchar(50) NOT NULL,
  `cep` varchar(9) NOT NULL,
  `ddi` varchar(3) NOT NULL,
  `ddd` varchar(3) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  PRIMARY KEY (`idFornecedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `produto` (
  `idProduto` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoria` int(11) NOT NULL,
  `idFornecedor` int(11) NOT NULL,
  `idFuncionario` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `precoCusto` float NOT NULL,
  `precoVenda` float NOT NULL,
  `quantidade` int(11) NOT NULL,
  `quantidadeMinima` int(11) NOT NULL,
  `quantidadeEstoque` int(11) NOT NULL,
  PRIMARY KEY (`idProduto`),
  KEY `idFornecedor` (`idFornecedor`),
  KEY `idCategoria` (`idCategoria`),
  KEY `idFuncionario` (`idFuncionario`),
  CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`idFornecedor`) REFERENCES `fornecedor` (`idFornecedor`) ON DELETE RESTRICT,
  CONSTRAINT `produto_ibfk_2` FOREIGN KEY (`idCategoria`) REFERENCES `categoria` (`idCategoria`) ON DELETE RESTRICT,
  CONSTRAINT `produto_ibfk_3` FOREIGN KEY (`idFuncionario`) REFERENCES `funcionario` (`idFuncionario`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `venda` (
  `idVenda` int(11) NOT NULL AUTO_INCREMENT,
  `idCliente` int(11) NOT NULL,
  `idFuncionario` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `valor` float NOT NULL,
  `dataVenda` date NOT NULL,
  PRIMARY KEY (`idVenda`),
  KEY `idCliente` (`idCliente`),
  KEY `idFuncionario` (`idFuncionario`),
  KEY `idProduto` (`idProduto`),
  CONSTRAINT `venda_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE,
  CONSTRAINT `venda_ibfk_2` FOREIGN KEY (`idFuncionario`) REFERENCES `funcionario` (`idFuncionario`) ON DELETE RESTRICT,
  CONSTRAINT `venda_ibfk_3` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `caixa` (
  `idCaixa` int(11) NOT NULL AUTO_INCREMENT,
  `idProduto` int(11) NOT NULL,
  `idVenda` int(11) NOT NULL,
  `precoUnitario` float NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`idCaixa`),
  KEY `idProduto` (`idProduto`),
  KEY `idVenda` (`idVenda`),
  CONSTRAINT `caixa_ibfk_1` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`) ON DELETE RESTRICT,
  CONSTRAINT `caixa_ibfk_2` FOREIGN KEY (`idVenda`) REFERENCES `venda` (`idVenda`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- FUNCTION, PROCEDURE AND TRIGGER
DELIMITER |

CREATE FUNCTION qtd_vendas_mes(mes INTEGER)
RETURNS INT
BEGIN
  DECLARE qtd INT;
  SELECT COUNT(idVenda) AS Quantidade_Vendas_Mes INTO qtd
  FROM venda
  WHERE (SELECT EXTRACT(MONTH FROM dataVenda)) = mes;
  RETURN qtd;
END;
|

CREATE PROCEDURE qtd_produto(nome_produto CHAR(20))
BEGIN
  SELECT produto.idProduto AS ID, produto.nome AS NOME_PRODUTO, produto.quantidade AS QUANTIDADE, fornecedor.razao AS FORNECEDOR, fornecedor.telefone AS CONTATO
  FROM produto, fornecedor
  WHERE produto.nome = nome_produto AND produto.idFornecedor = fornecedor.idFornecedor;
END;
|

DELIMITER ;
